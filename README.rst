als-ans-archappl
================

Ansible playbook to install `EPICS Archiver Appliance <http://slacmshankar.github.io/epicsarchiver_docs/index.html>`_ on CentOS 7.

Note that this playbook doesn't take care of firewall rules.
It assumes the firewall is not enabled. If you want to configure the firewall, you should do it via another playbook.

Variables
---------

Default variables are set in `group_vars/archiver_appliance`::

    epicsarchiverap_db_name: archappl
    epicsarchiverap_db_user: archappl
    epicsarchiverap_db_password: archappl

    mariadb_databases:
      - name: "{{ epicsarchiverap_db_name }}"
        init_script: create_archappl_tables.sql

    mariadb_users:
      - name: "{{ epicsarchiverap_db_user }}"
        password: "{{ epicsarchiverap_db_password }}"
        priv: "{{ epicsarchiverap_db_name }}.*:ALL"


This is the minimum to pass to create and initialize the database.

Check the `als-ans-role-mariadb <https://gitlab.com/als-ansible-galaxy/als-ans-role-mariadb>`_
and `als-ans-role-epicsarchiverap <https://gitlab.com/als-ansible-galaxy/als-ans-role-epicsarchiverap>`_ roles
for the complete list of variables you can change.


Usage
-----

The playbook can be run locally or from an Ansible server.

To run locally on the machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.7.0 already installed.
You should be able to ssh to the server to install and have sudo rights.

::

    $ git clone https://gitlab.com/als-ansible-galaxy/als-ans-archiverappliance.git
    $ cd als-ans-archiverappliance
    # Edit the "hosts" file
    # Change the database password in group_vars/archiver_appliance or create a host_vars/<hostname> file
    # with the variables to change
    $ make playbook


Testing
-------

The playbook is tested using `molecule <https://molecule.readthedocs.io/en/master/>`_ (version >= 2.19)::

    $ molecule test


License
-------

BSD 2-clause
